class Task {
    title: string = "";
    details: string = "";
    dueDate: Date = null;
    id:number=null;
    constructor(id:number, title: string, details: string, dueDate: Date) {
        this.dueDate = dueDate;
        this.title = title;
        this.details = details;
        this.id=id;
    }

    isOverDue = () => {
        if (this.dueDate) {
           return this.dueDate < new Date()
        } else {
            return false;
        }
    }
}

export default Task
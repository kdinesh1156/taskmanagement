import { Component, OnInit } from '@angular/core';
import { TaskListService } from './task-list.service';
import Task from './Task';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit {
  selectedTask: any = {};
  constructor(public taskListSrvc: TaskListService) {

  }

  selectTask(task: Task) {
    this.selectedTask = task
  }

  ngOnInit(): void {
  }

}

import { Injectable } from '@angular/core';
import Task from "./Task";
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TaskListService {
  tasksList: Task[] = [];

  private subject = new Subject<any>();

  constructor() {
    this.tasksList.push(new Task(1001, "Fix UI defect SDK-2123", "Production issue", new Date("06-03-2020")));
    this.tasksList.push(new Task(1002, "Customer entity creation SDK-3521", "New feature", new Date("01-06-2020")));
    this.tasksList.push(new Task(1003, "Environment setup for UAT", "Infrastructure", new Date("05-06-2020")));
    this.tasksList.push(new Task(1004, "Environment setup for QA", "Infrastructure", new Date("06-06-2020")));
    this.updateTaskCount()
  }

  getDueTasksCount() {
    return this.subject.asObservable();
  }

  updateTaskCount() {
    setTimeout(() => {
      this.subject.next(this.tasksList.filter((task) => {
        return task.isOverDue();
      }).length);
    }, 100)
  }

}

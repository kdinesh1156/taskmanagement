import { Component, OnInit } from '@angular/core';
import { TaskListService } from '../task-list/task-list.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  overDueTasks: number = 0;
  constructor(private taskListSrvc: TaskListService) {
    this.taskListSrvc.getDueTasksCount().subscribe((count) => {
      this.overDueTasks = count;
    })
  }

  ngOnInit(): void {
   
  }

}

import { Component, OnInit, Input } from '@angular/core';
import Task from '../task-list/Task';

@Component({
  selector: 'app-task-view',
  templateUrl: './task-view.component.html',
  styleUrls: ['./task-view.component.scss']
})
export class TaskViewComponent implements OnInit {
  @Input() task: Task;
  constructor() { }

  ngOnInit(): void {
    console.log(this.task);
  }

}
